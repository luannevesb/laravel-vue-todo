
// Importa todas as dependências

import axios from 'axios';
import Datepicker from 'vuejs-datepicker';
window.Vue = require('vue');
window.axios = axios;

// Carrega todos os componentes
Vue.component('task-list', require('./components/Task-list.vue').default);


// Define as instâncias VUE
const app = new Vue({
    el: '#app'
});
