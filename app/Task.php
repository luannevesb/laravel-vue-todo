<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $guarded = [];
    
    /**
     * Retorna as tarefas com o join da categoria
     */
    public static function returnTasks()
    {
        return Task::orderBy('id', 'desc')->with('category')->get();
    }

    public function category()
    {
        return $this->hasOne('App\Category', 'id', 'category_id');
    }
}
