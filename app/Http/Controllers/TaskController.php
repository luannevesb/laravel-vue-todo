<?php

namespace App\Http\Controllers;

use App\Task;
use App\Http\Requests\TaskRequest;

class TaskController extends Controller
{
    public function index()
    {
        return Task::returnTasks();
    }
    public function store(TaskRequest $request)
    {   
        $request = $request->validated();
        
        return Task::create(['body' => request('body'), 'category_id' => request('category_id')]);
    }
    public function edit(TaskRequest $request)
    {
        $id = $request->id;
        $request = (object)$request->validated();
        
        $task = Task::findOrFail($id);
        $task->body = $request->body;
        $task->category_id = $request->category_id;
        $task->save();
    }
    public function archive($id)
    {
        $task = Task::findOrFail($id);
        $task->archive = ! $task->archive;
        $task->save();
    }
    public function destroy($id)
    {
        $task = Task::findOrFail($id);
        $task->delete();
    }
}
