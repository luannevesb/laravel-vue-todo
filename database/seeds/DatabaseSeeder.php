<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'categories_name' => 'Trabalho',
            'created_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('categories')->insert([
            'categories_name' => 'Lazer',
            'created_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('categories')->insert([
            'categories_name' => 'Saúde',
            'created_at' => date("Y-m-d H:i:s")
        ]);
    }
}
